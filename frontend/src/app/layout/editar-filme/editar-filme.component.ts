import { Component, OnInit } from '@angular/core';
import { ApiService } from  '../../shared/services/api'
import { Router,ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-editar-filme',
  templateUrl: './editar-filme.component.html',
  styleUrls: ['./editar-filme.component.scss']
})
export class EditarFilmeComponent implements OnInit {

	form_edit={nome:'',descricao:'',categoria:'',categoria_id:''}
	url_id: string;
	categorias: Array<any> = [];

	constructor(private route: ActivatedRoute,private router: Router,private apiRest:ApiService){}

	ngOnInit(){
		this.url_id = this.route.snapshot.paramMap.get('id');
		this.getFilmeId()
		this.getFilmesById(this.url_id)
	}

    getFilmesById(id:string){
    	try{
	        this.apiRest.getFilmesById(id).subscribe(res => {
	            if(res['code'] == 200){
                    this.form_edit.nome = res.data.nome
                    this.form_edit.descricao = res.data.descricao
                    this.form_edit.categoria_id = res.data.categoria._id
                    this.form_edit.categoria = res.data.categoria
	            }
	        },
	        e => {
	            localStorage.clear();
	            this.router.navigate(['/login']);
	            //if(e.status == 401){}
	        })
		}catch(e){
			console.log(e)
		}
    }

    getFilmeId(){
    	try{
	        this.apiRest.getCategorias().subscribe(res => {
	            if(res['code'] == 200){
	                if(res.data.length > 0){
	                    this.categorias = Object.keys(res.data).map(i => res.data[i])
	                    //console.log(this.categorias)
	                }else{
	                    console.log('nao tem nada')
	                }
	            }
	        },
	        e => {
	            localStorage.clear();
	            this.router.navigate(['/login']);
	            //if(e.status == 401){}
	        })
		}catch(e){
			console.log(e)
		}
    }

    selecOptCat(cat){
    	this.form_edit.categoria = cat
    }

	editar(){
		//console.log(this.form_edit)

        this.apiRest.atualizarFilme(this.url_id, this.form_edit).subscribe(res => {
        	if(res['code'] == 200){
        		this.router.navigate(['/filmes']);
        	}else{
        		alert('Não foi possível cadastrar')
        	}
        },
        e => {
            localStorage.clear();
            this.router.navigate(['/login']);
            //if(e.status == 401){}
        })
	}

	voltar(){
		this.router.navigate(['/filmes']);
	}

}
