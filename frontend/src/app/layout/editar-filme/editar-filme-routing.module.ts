import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarFilmeComponent } from './editar-filme.component';

const routes: Routes = [
    {
        path: '',
        component: EditarFilmeComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarFilmeRoutingModule { }
