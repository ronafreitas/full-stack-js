import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarFilmeRoutingModule } from './editar-filme-routing.module';
import { EditarFilmeComponent } from '../editar-filme/editar-filme.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatInputModule, MatSelectModule} from '@angular/material';
import { FormsModule }   from '@angular/forms'; 

@NgModule({
  declarations: [EditarFilmeComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    EditarFilmeRoutingModule,
    FormsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class EditarFilmeModule { }
