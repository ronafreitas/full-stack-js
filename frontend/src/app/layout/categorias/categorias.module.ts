import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriasComponent } from './categorias.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatSelectModule, MatTableModule} from '@angular/material';
import { CategoriasRoutingModule } from './categorias-routing.module';
import { FormsModule }   from '@angular/forms'; 
@NgModule({
  declarations: [CategoriasComponent],
  imports: [
    CommonModule,
    CategoriasRoutingModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatSelectModule,
    MatTableModule,
    FormsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class CategoriasModule { }
