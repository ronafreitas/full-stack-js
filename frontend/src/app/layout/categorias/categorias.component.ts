import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ApiService } from  '../../shared/services/api'
import { Router } from '@angular/router';

export interface Categoria {
    nome: string;
}

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

    displayedColumns = ['nome'];
    dataSource = new MatTableDataSource([]);
    dataout:Categoria[]

	constructor(private router: Router,private apiRest:ApiService){}

    ngOnInit(){
        // preload
        this.getCategorias()
    }

    getCategorias(){
        this.apiRest.getCategorias().subscribe(res => {
            if(res['code'] == 200){
                if(res.data.length > 0){
                    this.dataout = Object.keys(res.data).map(i => res.data[i])
                    this.dataSource = new MatTableDataSource(this.dataout);
                }else{
                    console.log('nao tem nada')
                }
            }
        },
        e => {
            localStorage.clear();
            this.router.navigate(['/login']);
            //if(e.status == 401){}
        })
    }

    novaCategoria(){
        this.router.navigate(['/cadastro-categorias']);
    }
}
