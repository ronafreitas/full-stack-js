import { Component } from '@angular/core';
import { ApiService } from  '../../shared/services/api'
import { Router } from '@angular/router'

export interface Categoria {
	nome: string;
}

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro-categorias.component.html',
  styleUrls: ['./cadastro-categorias.component.scss']
})
export class CadastroCategoriasComponent {

	form_cad_cat:Categoria={nome:''}

	constructor(private router: Router,private apiRest:ApiService){}

  trimming_fn(x){
    return x ? x.replace(/^\s+|\s+$/gm, '') : '';
  }

	salvar(){
    const nome = this.trimming_fn(this.form_cad_cat.nome)

    if(nome.length < 1){
      let element=document.getElementById('nomeElem');
      element.focus();  
      return false
    }

    this.apiRest.criarCategoria(this.form_cad_cat).subscribe(res => {
    	if(res['code']==200){
    		this.router.navigate(['/categorias']);
    	}else{
    		alert('Erro ao tentar adicionar')
    	}
    },
    e => {            
        localStorage.clear();
        this.router.navigate(['/login']);
    })
	}

  voltar(){
    this.router.navigate(['/categorias']);
  }

}
