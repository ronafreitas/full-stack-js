import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroCategoriasComponent } from './cadastro-categorias.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatSelectModule} from '@angular/material';
import { CadastroCategoriasRoutingModule } from './cadastro-categorias-routing.module';
import { FormsModule }   from '@angular/forms'; 
@NgModule({
  declarations: [CadastroCategoriasComponent],
  imports: [
    CommonModule,
    CadastroCategoriasRoutingModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class CadastroCategoriasModule { }
