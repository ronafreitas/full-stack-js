import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastroCategoriasComponent } from './cadastro-categorias.component';

const routes: Routes = [
    {
        path: '',
        component: CadastroCategoriasComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CadastroCategoriasRoutingModule {}
