import { Component, OnInit } from '@angular/core'
import { ApiService } from  '../../shared/services/api'
import { Router } from '@angular/router'

export interface Filme {
	nome: string;
	descricao: string;
	categoria: string;
}

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro-filmes.component.html',
  styleUrls: ['./cadastro-filmes.component.scss']
})
export class CadastroFilmesComponent implements OnInit {

	form_cad:Filme={nome:'',descricao:'',categoria:''}
	categorias: Array<any> = [];

	constructor(private router: Router,private apiRest:ApiService){}

	ngOnInit(){
		this.getCategorias()
	}

    getCategorias(){

    	// criar tratamento de errors 

    	try{
	        this.apiRest.getCategorias().subscribe(res => {
	            if(res['code'] == 200){
	                if(res.data.length > 0){
	                    this.categorias = Object.keys(res.data).map(i => res.data[i])
	                }else{
	                    console.log('nao tem nada')
	                }
	            }
	        },
	        e => {
	            localStorage.clear();
	            this.router.navigate(['/login']);
	            //if(e.status == 401){}
	        })
		}catch(e){
			console.log(e)
		}
    }

	salvar(){
        this.apiRest.criarFilme(this.form_cad).subscribe(res => {
        	if(res['code'] == 200){
        		this.router.navigate(['/filmes']);
        	}else{
        		alert('Não foi possível cadastrar')
        	}
        },
        e => {
            localStorage.clear();
            this.router.navigate(['/login']);
            //if(e.status == 401){}
        })
	}
	voltar(){
		this.router.navigate(['/filmes']);
	}
}
