import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroFilmesComponent } from './cadastro-filmes.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatSelectModule} from '@angular/material';
import { CadastroFilmesRoutingModule } from './cadastro-filmes-routing.module';
import { FormsModule }   from '@angular/forms'; 
@NgModule({
  declarations: [CadastroFilmesComponent],
  imports: [
    CommonModule,
    CadastroFilmesRoutingModule,
    MatInputModule,
    MatCheckboxModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    FlexLayoutModule.withConfig({addFlexToParent: false})
  ]
})
export class CadastroFilmesModule { }
