import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {path: '',redirectTo: 'filmes'},
            {path: 'filmes',loadChildren: './filmes/filmes.module#FilmesModule'},
            {path: 'cadastro-filmes',loadChildren: './cadastro-filmes/cadastro-filmes.module#CadastroFilmesModule'},
            {path: 'editar-filme', redirectTo: 'filmes'},
            {path: 'editar-filme/:id',loadChildren: './editar-filme/editar-filme.module#EditarFilmeModule'},
            {path: 'categorias',loadChildren: './categorias/categorias.module#CategoriasModule'},
            {path: 'comentarios',loadChildren: './comentarios/comentarios.module#ComentariosModule'},
            {path: 'cadastro-categorias',loadChildren: './cadastro-categorias/cadastro-categorias.module#CadastroCategoriasModule'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
