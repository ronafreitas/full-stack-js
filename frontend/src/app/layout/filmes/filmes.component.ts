import { Component, OnInit } from '@angular/core';

import { MatTableDataSource } from '@angular/material';
import { ApiService } from  '../../shared/services/api'
import { Router } from '@angular/router';

export interface Filmes {
    nome: string;
    descricao: string;
    categoria: string;
}

@Component({
    selector: 'app-filmes',
    templateUrl: './filmes.component.html',
    styleUrls: ['./filmes.component.scss']
})
export class FilmesComponent implements OnInit {

    displayedColumns = ['nome', 'descricao', 'categoria','editButton'];
    dataSource = new MatTableDataSource([]);
    dataout:Filmes[]
    //dataout: Array<any> = [];

    constructor(private router: Router,private apiRest:ApiService){}

    ngOnInit(){
        this.getFilmes()
    }

    getFilmes(){
        this.apiRest.getFilmes().subscribe(res => {

            /*

            _id: "5dc833c2869f0a0ea0eb4496"
            ​categoria: {…}
            ​​_id: "5dc71fd70dd5c521819d1af7"
            ​​nome: "ação"
            ​​<prototype>: Object { … }
            ​descricao: "esse é um filme incrível, super legal, eu adoro"
            ​nome: "algum filme"

            const comt = {
                comentario:'teste comentario'
                filme: res.data[0]
            }
            */
            //this.salvarComentario(comt)

            if(res['code'] == 200){
                if(res.data.length > 0){
                    this.dataout = Object.keys(res.data).map(i => res.data[i])
                    this.dataSource = new MatTableDataSource(this.dataout);
                }else{
                    console.log('nao tem nada')
                }
            }
        },
        e => {
            localStorage.clear();
            this.router.navigate(['/login']);
        })
    }

    novoFilme(){
        this.router.navigate(['/cadastro-filmes']);
    }

    editar(_id:string){
        this.router.navigate(['/editar-filme/'+_id]);
    }

    apagar(_id:string,nome:string,e){
        var cnf = confirm(`Confirma apagar o filme '${nome}'?`);
        if(cnf == true){
            this.apiRest.deletarFilme(_id).subscribe(res => {
                if(res['code'] == 200){
                    e.target.parentNode.parentNode.parentNode.remove()
                }
            },
            e => {
                localStorage.clear();
                this.router.navigate(['/login']);
            })
        }
    }

}
