import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ApiService } from  '../../shared/services/api'
import { Router } from '@angular/router';

@Component({
    selector: 'app-comentarios',
    templateUrl: './comentarios.component.html',
    styleUrls: ['./comentarios.component.scss']
})
export class ComentariosComponent implements OnInit {

    displayedColumns = ['comentario', 'filme'];
    dataSource = new MatTableDataSource([]);
    //dataout:Comentario[]
    dataout: Array<any> = [];

    constructor(private router: Router,private apiRest:ApiService){}

    ngOnInit(){
        this.getComentarios()
    }

    getComentarios(){
        this.apiRest.getComentarios().subscribe(res => {
            if(res['code'] == 200){
                if(res.data.length > 0){
                    this.dataout = Object.keys(res.data).map(i => res.data[i])
                    this.dataSource = new MatTableDataSource(this.dataout);
                }else{
                    console.log('nao tem nada')
                }
            }
        },
        e => {
            localStorage.clear();
            this.router.navigate(['/login']);
        })
    }

    atualizar(){
        this.getComentarios()
    }
}
