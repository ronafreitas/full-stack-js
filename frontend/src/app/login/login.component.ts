import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth'
//import { Observable } from "rxjs"

export interface User {
	email: string;
	password: string;
}

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	usAcc:User
	//isLoggedIn:Observable<boolean>
	formulario:any={email:'',senha:''}
	subscription:any
	msgAcesso:string=''

	constructor(private router: Router,private auth:AuthService){}

	ngOnInit(){
		// implementar redirecionamento IF LOGGED
		localStorage.clear(); 
	}
	
	acessar(){
		
		// implementar validações

		this.usAcc = {email:this.formulario.email,password:this.formulario.senha}
		this.auth.login(this.usAcc).subscribe(res => {
			if(res['code'] == 200){
				localStorage.setItem('isLoggedin', 'true');
				localStorage.setItem("user_app", JSON.stringify(res))
				//this.router.navigateByUrl('dashboard')
				this.router.navigate(['/filmes']);
			}else{
				this.msgAcesso = 'Não foi possível acessar, email ou senha incorretos'
			}
		},
		e => {
			// verificar código de status da requisição
			this.msgAcesso = 'Não foi possível acessar, pedimos desculpas por isso'
		});
	}

	cadastrar(){
		this.router.navigate(['/register']);
	}
}
