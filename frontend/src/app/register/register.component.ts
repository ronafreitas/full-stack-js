import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth'

export interface User {
	name: string;
	email: string;
	password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

	usAcc:User
	formulario:any={name:'',email:'',senha:''}
	msgAcesso:string=''

	constructor(private router: Router,private auth:AuthService){}

	ngOnInit(){
		/*


	

				ENVIAR EMAIL QUANDO FIZER CADASTRO

				email simples, só informativo




		*/
	}

	cadastrar(){
		this.usAcc = {name:this.formulario.nome,email:this.formulario.email,password:this.formulario.senha}
		this.auth.create(this.usAcc).subscribe(res => {
			console.log(res)
			if(res['code'] == 200){
				let self = this
				setTimeout(function(){ self.router.navigate(['/login']); }, 1500);
				this.msgAcesso = 'Cadastro realizado com sucesso'
			}else{
				this.msgAcesso = 'Cadastro realizado com sucesso'
			}

		},
		e => {
			// verificar código de status da requisição
			this.msgAcesso = 'Não foi possível acessar, pedimos desculpas por isso'
		});
	}

	login(){
		this.router.navigate(['/login']);
	}
}
