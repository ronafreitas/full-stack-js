import { Injectable } from  '@angular/core'
import { HttpClient, HttpHeaders} from  '@angular/common/http'
import { tap } from  'rxjs/operators'
import { Observable } from  'rxjs'
import { environment as ENV} from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL:string = ENV.apiURL

  constructor(private httpClient:HttpClient){}

  // Header
  setHeader(){
    const usap = JSON.parse(localStorage.getItem('user_app')) || null;
    if(usap['data'] != undefined){
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': usap.data.token
      });
    }else{
      return null
    }
  }

  // Filmes
  getFilmes():Observable<any>{
    return this.httpClient.get(`${this.apiURL}/filmes`,{ headers: this.setHeader() }).pipe(
      tap(async (res) => {
        await res
      })
    )
  }
  getFilmesById(_id:string):Observable<any>{
    return this.httpClient.get(`${this.apiURL}/filmes/${_id}`,{ headers: this.setHeader() }).pipe(
      tap(async (res) => {
        await res
      })
    )
  }
  criarFilme(filme:any):Observable<any>{
    return this.httpClient.post<any>(`${this.apiURL}/filmes`, filme, { headers: this.setHeader() }).pipe(
      tap(async (res:any) => {
        if(res){
          await res
        }
      })
    );
  }
  atualizarFilme(_id:string,filme:any):Observable<any>{
    return this.httpClient.put<any>(`${this.apiURL}/filmes/${_id}`, filme, { headers: this.setHeader() }).pipe(
      tap(async (res:any) => {
        if(res){
          await res
        }
      })
    );
  }
  deletarFilme(_id:string):Observable<any>{
    return this.httpClient.delete<any>(`${this.apiURL}/filmes/${_id}`, { headers: this.setHeader() }).pipe(
      tap(async (res:any) => {
        if(res){
          await res
        }
      })
    );
  }

  // Categoria
  getCategorias():Observable<any>{
    return this.httpClient.get(`${this.apiURL}/categorias`,{ headers: this.setHeader() }).pipe(
      tap(async (res) => {
        await res
      })
    )
  }
  criarCategoria(categoria:any):Observable<any>{
    return this.httpClient.post<any>(`${this.apiURL}/categorias`, categoria, { headers: this.setHeader() }).pipe(
      tap(async (res:any) => {
        if(res){
          await res
        }
      })
    );
  }


  getComentarios():Observable<any>{
    return this.httpClient.get(`${this.apiURL}/comentarios`,{ headers: this.setHeader() }).pipe(
      tap(async (res) => {
        await res
      })
    )
  }

  /*
  criarComentario(comentario:any):Observable<any>{
    return this.httpClient.post<any>(`${this.apiURL}/comentarios`, comentario, { headers: this.setHeader() }).pipe(
      tap(async (res:any) => {
        if(res){
          await res
        }
      })
    );
  }
  */

}